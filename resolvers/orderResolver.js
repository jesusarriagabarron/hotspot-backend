//Load database dynamodb functions
import myTable from './../db/orderTable';
import userTable from'./../db/userTable';
import driverTable from './../db/driverTable';

exports.resolver = {
        Query: {
            order: (parent, args, context) => {
                return myTable.getOrder(args.id);
            },
            allOrders: (parent, args, context) => {
                return myTable.getAllOrders();
            }
        },
        Mutation: {
            submitOrder: (parent, args, context) => {
                console.log('mutation finished')
                return myTable.submitOrder(args.input);
            }
        },
        Order: {
            driver: async (parent, args, context) => {
                return await driverTable.getDriver(parent.driver);
            },
            user: async (parent, args, context) => {
                console.log('getting order-user');
                const user =  await userTable.getUser(parent.user);
                console.log('data- user ');
                console.log(user);
                return user;
            }
        }
    }