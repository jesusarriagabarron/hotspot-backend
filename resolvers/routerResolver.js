//Load database dynamodb functions
import routerTable from './../db/routerTable';
//import uniqid from 'uniqid';



exports.resolver =  {
    Query:{
        getRouter:(parent,args, context) => {
            return routerTable.getRouter(args.id);
        }
    },
    Mutation:{
        addRouter:(parent, args, context) => {
            console.log('mutation finished')
            return routerTable.addRouter(args.input);
        }
    },
    User:{
       
    }
}
