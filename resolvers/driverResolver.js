//Load database dynamodb functions
import table from './../db/driverTable';
import uniqid from 'uniqid';

exports.resolver = {
    Query:{
        driver:(parent,args, context) => {
            return table.getDriver(args.id);
        }
    },
    Mutation: {
        signupDriver:(parent, args, context) => {
            console.log('mutation createDriver finished')
            console.log(args);
            return table.signupDriver(args.input);
        }
    },
    Driver:{
        
    }
}

