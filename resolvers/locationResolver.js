//Load database dynamodb functions
import myTable from'./../db/locationTable';


exports.resolver = {
    Query:{
        location:(parent,args, context) => {
            return myTable.getLocation(args.id);
        }
    },
    Mutation:{

    },
    Location:{

    }
}

