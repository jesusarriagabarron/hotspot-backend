//Load database dynamodb functions
import userTable from './../db/userTable';
import uniqid from 'uniqid';


exports.resolver =  {
    Query:{
        allUsers:  (parent, args, context) => {
            return userTable.getUsers();
        },
        user:(parent,args, context) => {
            return userTable.getUser(args.id);
        }
    },
    Mutation:{
        signupUser:(parent, args, context) => {
            console.log('mutation finished')
            return userTable.signupUser(args.input);
        }
    },
    User:{
       
    }
}
