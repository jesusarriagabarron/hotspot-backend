// Load the SDK for JavaScript
import AWS from 'aws-sdk';
import faker from 'faker';
import uniqid from 'uniqid';
import moment from 'moment';
import removeAccents from 'remove-accents';
faker.locale = "es_MX";

// .env  configuration file
const dotenv = require('dotenv');
dotenv.config();
// AWS key
AWS.config.update({
    accessKeyId: process.env.AWS_KEY, secretAccessKey: process.env.AWS_SECRET, region: 'us-east-1'
  });

//load dynamo client 
const docClient = new AWS.DynamoDB.DocumentClient();

//fake data just for fun!
let seedUsers = async() =>{
    for(var i=0;i<20; i++){
        let params = {
            TableName: 'User',
            Item:{
                id: uniqid('u-'),
                email: faker.internet.email().toLowerCase(),
                firstName: faker.name.firstName(),
                lastName: faker.name.lastName(),
                dob: moment(faker.date.between('1980-01-01','2005-12-12')).toISOString(30),
                phone: faker.phone.phoneNumber(),
                memberSince: moment(faker.date.between('2019-01-01','2019-08-01')).toISOString(10),
                rating: faker.random.number({min:0,max:5}),
                disabled: faker.random.boolean(),
            }
        }
        await docClient.put(params,(err,data)=>{
            if(err)
            console.log(err);
            console.log(data);
        });
    }
}

seedUsers();