//Schema for graphql
import {gql} from 'apollo-server-express';
import moment from 'moment';


exports.typeDef = gql `

extend type Query{
  location( id:String! ): Location
}

type Location {
  id: ID!
  name: String
  lat: String
  lng: String
  addressText:String
  user: User
  photo:String
}

`