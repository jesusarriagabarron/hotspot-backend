//Schema for graphql
import {gql} from 'apollo-server-express';
import moment from 'moment';


exports.typeDef = gql `

type Payment {
  id: ID!
  name: String
  token: String
}


`