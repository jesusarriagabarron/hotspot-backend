//Schema for graphql

/* review Driver and User after request */


import {gql} from 'apollo-server-express';
import moment from 'moment';


exports.typeDef = gql `

extend type Query {
  allOrders: [Order]
  order(id:String):Order!
}

type Order {
  id: ID!
  driver:Driver
  user:User!
  orderDetails: String!
  delivery:[String]!
  pickup:Location
  pickupAddress:String
  status:String
  transaction:Transaction
  ticket: String
  timestamp: Int
}

input OrderInput{
  driver: String
  user: String!
  orderDetails: String!
  delivery: [String]!
  pickup: String
  pickupAddress: String
  status: String = "new"
  ticket: String = "http://www.photolibrary.com/myphoto.jpg"
  timestamp: String = "${moment().unix()}"
}

extend type Mutation{
  submitOrder( input: OrderInput ): Order
}

`