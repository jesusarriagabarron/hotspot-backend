//Schema for graphql
import {gql} from 'apollo-server-express';
import moment from 'moment';


exports.typeDef = gql `

type Transaction {
  id: ID!
  order: Order!
  total: Float!
  tip: Float
  taxes: Float
  timestamp: Int
}

`