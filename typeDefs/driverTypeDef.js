//Schema for graphql
import {gql} from 'apollo-server-express';
import moment from 'moment';


exports.typeDef = gql `

extend type Query {
  driver(id:String):Driver!
}

input DriverInput{
  email: String!
  firstName: String!
  lastName: String!
  dob: String!
  phone: String!
  memberSince: String = "${moment().toISOString()}"
  disabled: Boolean = false
  driverLicense: String
}

extend type Mutation{
  signupDriver( input: DriverInput ): Driver
}

type Driver {
  id: ID!
  email: String!
  firstName: String!
  lastName: String!
  dob: String!
  phone: String!
  memberSince: String!
  rating: Float
  disabled: Boolean!
  driverLicense: String!
  active: Boolean!
  withdraw: [Payment]
}



`