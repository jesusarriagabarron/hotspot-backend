//Schema for graphql
/*

TODO password add jwt to signup 

*/


import {gql} from 'apollo-server-express';
import moment from 'moment';

exports.typeDef = gql `

extend type Query{
    getRouter( id:String! ): Router!
}

input RouterInput{
  ip: String!
  username: String!
  password: String!
  name: String!
  owner: String
}


type Router {
  ip: String!
  username: String!
  password: String
  name: String!
  owner: User
}

extend type Mutation{
  addRouter( input: RouterInput ): Router
}

`