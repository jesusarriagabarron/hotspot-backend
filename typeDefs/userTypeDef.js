//Schema for graphql
/*

TODO password add jwt to signup 

*/


import {gql} from 'apollo-server-express';
import moment from 'moment';

exports.typeDef = gql `

extend type Query{
    allUsers: [User]
    user( id:String! ): User!
}

input UserInput{
  firstName: String!
  lastName: String!
  email: String!
  phone: String!
  memberSince: String = "${moment().toISOString()}"
  disabled: Boolean = false
  active: Boolean = true
}


type User {
  id: ID!
  email: String!
  firstName: String!
  lastName: String!
  dob: String
  phone: String!
  memberSince: String!
  rating: Float
  disabled: Boolean!
  payments: [Payment]
  addresses: [Location]
  orders: [Order]
}

extend type Mutation{
  signupUser( input: UserInput ): User
}

`