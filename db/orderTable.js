//User Model instance
import Order from '../models/order';


//get only one order given the ID
const getOrder = (id) => {
    return new Promise((resolve, reject) => {
        Order.findOne({
            id: id
        }, function (err, order) {
            console.log(order);
            resolve(order);
        });
    });
}

const getAllOrders = () =>{
    return new Promise( (resolve,reject)=>{
        Order.find({},(err,orders)=>{
            resolve(orders);
        })
    })
}

const submitOrder = (args) => {
    return new Promise((resolve, reject) => {
        let order = new Order(args);
        order.save()
            .then(response => {
                console.log(response);
                resolve(response);
            })
            .catch(err => {
                console.log(err);
                reject(err);
            });
    });
}


//expose getUsers
module.exports = {
    getOrder,
    submitOrder,
    getAllOrders
}