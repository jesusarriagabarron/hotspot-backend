//User Model instance
import Driver from '../models/driver';

//get only one Driver given the ID
const getDriver = (id)=>{
    return new Promise( (resolve,reject)=>{
       Driver.findOne({_id:id}, (err,driver) => {
            if(err){console.log(err),reject(err)}
            resolve(driver);
       });
    });
}


// Signup a driver
const signupDriver = ( args ) =>{
    return new Promise( (resolve, reject)=>{
        let model = new Driver(args);
        model.save()
        .then( response =>{
            resolve(response);
        })
        .catch( err => {
            console.log(err);
            reject(err);
        });
    })
}

//expose getUsers
module.exports = {
    getDriver,
    signupDriver
}