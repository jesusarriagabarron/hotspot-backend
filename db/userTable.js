//User Model instance
import User from '../models/user';

//get all users from dynamodb
const getUsers =  () => {
    return new Promise( (resolve, reject)=>{
        User.find({}, (err,_users ) =>{
            if(err){reject(err)}
            resolve(_users);
        });
    })
}

//get only one user given the ID
const getUser = (id) => {
    return new Promise( (resolve,reject) => {
      User.findById({_id:id}, (err, _user) => { 
          if(err){console.log(err);reject(err)}
          if(_user){
            resolve(_user);
          }
         
      })
    });
}

const signupUser = ( args ) =>{
    return new Promise( (resolve, reject)=>{
        let user = new User(args);
        user.save()
        .then( response =>{
            console.log(response);
            resolve(response);
        })
        .catch( err => {
            console.log(err);
            reject(err);
        });
    })
}

//expose getUsers
module.exports = {
    getUsers,
    getUser,
    signupUser
}