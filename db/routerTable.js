//User Model instance
import Router from '../models/router';
import mongoose from 'mongoose'

const crypto = require('crypto');
const dotenv = require('dotenv');
dotenv.config();


//get all users from dynamodb
const getRouters = () => {
    return new Promise((resolve, reject) => {
        Router.find({}, (err, _routers) => {
            if (err) {
                reject(err)
            }
            resolve(_routers);
        });
    })
}

//get only one user given the ID
const getRouter = (id) => {
    return new Promise((resolve, reject) => {
        Router.findById({
            _id: id
        }, (err, _router) => {
            if (err) {
                console.log(err);
                reject(err)
            }
            if (_router) {
                _router = _router.toObject();
                _router.password
                console.log(_router);
                delete _router.password;
                // let pw = _router.password;
                // decipher.write(pw, 'hex');
                // decipher.end();
                // _router.password = decrypted;
                resolve(_router);
            }

        })
    });
}

const addRouter = (args) => {
    return new Promise((resolve, reject) => {
        cipher.write(args.password);
        cipher.end();
        args.password = encrypted;
        let router = new Router(args);
        router
            .save()
            .then(response => {
                console.log(response);
                resolve(response);
            })
            .catch(err => {
                console.log(err);
                reject(err);
            });
    })
}

//expose getUsers
module.exports = {
    getRouters,
    getRouter,
    addRouter
}










/*******  Cipher Decipher */


const algorithm = 'aes-192-cbc';
const password = process.env.DES;
const key = crypto.scryptSync(password, 'salt', 24);
const iv = Buffer.alloc(16, 0);
const cipher = crypto.createCipheriv(algorithm, key, iv);

let encrypted = '';
cipher.on('readable', () => {
    let chunk;
    while (null !== (chunk = cipher.read())) {
        encrypted += chunk.toString('hex');
    }
});
cipher.on('end', () => {});

const decipher = crypto.createDecipheriv(algorithm, key, iv);

let decrypted = '';
decipher.on('readable', () => {
    let chunk;
    while (null !== (chunk = decipher.read())) {
        decrypted += chunk.toString('utf8');
    }
});
decipher.on('end', () => {
    // Prints: some clear text data
});