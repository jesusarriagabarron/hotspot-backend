//Resolvers for graphql
import glob from 'glob';
import _ from 'lodash';

let MainResolver = [];

glob.sync('./resolvers/*.js').forEach(file => {
  let resolverFile = require(file);
 
  MainResolver = _.concat(MainResolver, resolverFile.resolver);
});

module.exports = MainResolver;