import mongoose from 'mongoose';
let Schema = mongoose.Schema;

let DriveSchema = new Schema({
    email: String,
    firstName: String,
    lastName: String,
    dob: String,
    phone: String,
    memberSince: String,
    password: String,
    failedLoginAttempts: Number,
    rating: Schema.Types.Decimal128,
    disabled: Boolean,
    active: Boolean,
    withdraw: [],
    driverLicense:String
});


let Model = null;
try {
    Model = mongoose.model('Driver', DriveSchema);
} catch {
    Model = mongoose.model('Driver');
}

module.exports =  Model;