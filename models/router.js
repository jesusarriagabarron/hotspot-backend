import mongoose from 'mongoose'

let Schema = mongoose.Schema;

let RouterSchema = new Schema({
    name: String,
    ip: String,
    username: String,
    password: String,
    owner: mongoose.SchemaTypes.ObjectId
});



let Model = null;
try {
    Model = mongoose.model('Router', RouterSchema);
} catch {
    Model = mongoose.model('Router');
}

module.exports =  Model;