import mongoose from 'mongoose';
let Schema = mongoose.Schema;

let UserSchema = new Schema({
    email: String,
    firstName: String,
    lastName: String,
    password: String,
    failedLoginAttempts: Number,
    dob: String,
    phone: String,
    memberSince: String,
    rating: Schema.Types.Decimal128,
    disabled: Boolean,
    payments: [],
    addresses: [],
    orders: []
});

let User = null;
try {
    User = mongoose.model('User', UserSchema);
} catch {
    User = mongoose.model('User');
}

module.exports =  User;