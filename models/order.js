import mongoose from 'mongoose';
let Schema = mongoose.Schema;

let OrderSchema = new Schema({
    driver: mongoose.SchemaTypes.ObjectId,
    user: mongoose.SchemaTypes.ObjectId,
    orderDetails: String,
    delivery:[String],
    pickup: mongoose.SchemaTypes.ObjectId,
    pickupAddress:String,
    status:String,
    transaction: mongoose.SchemaTypes.ObjectId,
    ticket: String, // photo link
    timestamp: Number
});

let Model = null;
try {
    Model = mongoose.model('Order', OrderSchema);
} catch {
    Model = mongoose.model('Order');
}

module.exports =  Model;