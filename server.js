//apollo express
import {ApolloServer,gql} from 'apollo-server-express';
//load schema
import {typeDefs} from './apolloTypeDefs';
//load resolvers
import resolvers from './apolloResolvers';
import serverless from 'serverless-http';
import express from 'express';
import bodyParser from 'body-parser';
const  app = express();
import cors from 'cors';
import dotenv from 'dotenv';
import mongoose from 'mongoose';

dotenv.config();

const startServer = async () => {

  app.use(bodyParser.json({
    strict: false,
    limit: '20mb'
  }));
  
  //cors
  app.use(cors());
  
  //Database connection Mongo
  mongoose.Promise = global.Promise;
  console.log(process.env.DATABASE_URL);
  await mongoose.connect(process.env.DATABASE_URL, {
    useNewUrlParser: true,
    useUnifiedTopology:true,
   // ssl: true
  }).then(() => {
    console.log('ok connected');
  }).catch(err => { // we will not be here...
    console.error('App starting error:', err.stack);
    process.exit(1);
  });
  
  //initialize Apollo Server
  const server = new ApolloServer({
    typeDefs,
    resolvers,
    playground: true
  });
  
  //middleware for express
  server.applyMiddleware({
    app
  });
  
  //routes if needed
  app.route('/robles-test').get((req, res) => {
    console.log('ok');
    res.json({
      success: 'hola mundo'
    });
  });
  return app;
}

startServer();



const port = 3044

app.listen(port);
console.log(`listening on http://localhost:${port}`);
