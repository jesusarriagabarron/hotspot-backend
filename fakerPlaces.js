// Load the SDK for JavaScript
let AWS = require('aws-sdk');
let faker = require('faker');
let uniqid = require('uniqid');
const moment = require('moment');
const removeAccents = require('remove-accents');
faker.locale = "es_MX";
const gkey = 'AIzaSyArbpUG6Ben3xFF1yjLA6aFq9tOULbSmWg';
const googleMapsClient = require('@google/maps').createClient({
    key: gkey,
    Promise: Promise
});

// .env  configuration file
const dotenv = require('dotenv');
dotenv.config();
// AWS key
AWS.config.update({
    accessKeyId: process.env.AWS_KEY,
    secretAccessKey: process.env.AWS_SECRET,
    region: 'us-east-1'
});

//load dynamo client 
const docClient = new AWS.DynamoDB.DocumentClient();

//fake data just for fun!
let seedUsers = async () => {
    for (var i = 0; i < 20; i++) {
        let params = {
            TableName: 'User',
            Item: {
                id: uniqid('u-'),
                email: faker.internet.email().toLowerCase(),
                firstName: faker.name.firstName(),
                lastName: faker.name.lastName(),
                dob: moment(faker.date.between('1980-01-01', '2005-12-12')).toISOString(30),
                phone: faker.phone.phoneNumber(),
                memberSince: moment(faker.date.between('2019-01-01', '2019-08-01')).toISOString(10),
                rating: faker.random.number({
                    min: 0,
                    max: 5
                }),
                disabled: faker.random.boolean(),
            }
        }
        await docClient.put(params, (err, data) => {
            if (err)
                console.log(err);
            console.log(data);
        });
    }
}

//seedUsers();
googleMapsClient.placesNearby({
        location: '18.462062, -97.3948337',
        radius: 1000,
        language: 'es-419',
        type: 'restaurant'
    }).asPromise()
    .then(async response => {
        let items = response.json.results;
        let resultsMap = items.map( item => {
            return {
                placeId: item.place_id,
                name: item.name,
                photo:item.photos && item.photos.length > 0?
                'https://maps.googleapis.com/maps/api/place/photo?maxwidth=600&photoreference=' + item.photos[0].photo_reference + '&key=' + gkey:
                'no-photo',
                rating:item.rating,
                address:item.vicinity,
                lat:item.geometry.location.lat,
                lng:item.geometry.location.lng
            }
        })
        console.log(resultsMap);
        for(let i=0;i<resultsMap.length;i++){
            let params = {
                TableName: 'Location',
                Item: {
                    id: resultsMap[i].placeId,
                    name:resultsMap[i].name,
                    photo:resultsMap[i].photo,
                    addressText:resultsMap[i].address,
                    lat: resultsMap[i].lat,
                    lng: resultsMap[i].lng
                }
            }
            await docClient.put(params, (err, data) => {
                if (err)
                    console.log(err);
                console.log(data);
            });
        }
    }).catch(err => {
        console.log(err)
    });