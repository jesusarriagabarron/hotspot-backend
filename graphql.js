import app from './server';
import serverless from 'serverless-http';

module.exports.handler = serverless(app);
