//Schema for graphql
import {ApolloServer,gql}  from 'apollo-server-express';
import glob from 'glob';

const typeDefinitions = gql ` 

type Query {
   hello: String 
}

type Mutation{
  _empty:String
}

`;


let typeDefArray = [typeDefinitions];
glob.sync('./typeDefs/*.js').forEach( file =>{
  typeDefArray.push(require(file).typeDef);
});

// Export the array with all definitions
exports.typeDefs = typeDefArray